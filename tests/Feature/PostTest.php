<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    public function testCanView()
    {
        $response = $this->get('/post/5a9d0f8e9a892012bb2ed465');

        $response->assertStatus(200);
    }

    public function testCanSaveAsGuest()
    {
        $response = $this->post('/post/5a9d0f8e9a892012bb2ed465');
        $response->assertStatus(405);
    }
}
