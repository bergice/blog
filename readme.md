# Laravel, MongoDB Blog

This is a blog webapp built in Laravel which uses MongoDB to store the blog posts and CKEditor to write posts. Laravel Blade is used for the front-end. 

![show case 1](https://i.imgur.com/EP1FV62.png)
![show case 2](https://i.imgur.com/8CoZeHL.png)

## Configuration

This application assumes the environment is setup with the following:

* MySQL database
* MongoDB database

> Note: This was originally built in [Homestead](https://laravel.com/docs/5.6/homestead), as it comes with all the requirements out of the box, except for MongoDB.

There is a blank `.env-example` file in the root directory you can rename to `.env` and fill out the environment variables with your configuration in order to run this.

## Built with

* **PHP 7.2**
* **Laravel 5.5**
* **MongoDB 2.6** for storing blog post data
* **MySQL 14.14** for user data
* **CKEditor** to edit blog post content

## Features

* View, edit, create and delete blog posts. Any user can view blog posts.
* User authentication
* WYSIWYG blog post editor
* Seed data from [https://www.articlecity.com](https://www.articlecity.com)
* Localization
* Validation

## To-Do

These are some improvements/features that could be worked on from here:

* Tasks
    * Unit tests - there are some, but we need more.
    * VueJs/React framework in order to get finer control over the front-end components, less cluttered code, and it will make it easier to create asynchronous components.
* Features
    * Comments
    * Social Media Share Buttons
    * Admin panel
        * Administer users/roles
        * Site title, description, styling
    * Other cool features, such as a timeline sidebar for navigation, something like [this](https://ant.design/components/timeline/).