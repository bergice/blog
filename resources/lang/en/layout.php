<?php

return [

    'new_post' => 'New Post',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'toggle_navigation' => 'Toggle Navigation',

];
