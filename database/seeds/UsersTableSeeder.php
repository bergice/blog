<?php

use Blog\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        factory(User::class, 1)->create([
            'name' => 'admin',
            'password' => Hash::make('admin'),
            'email' => 'admin@website.com',
        ]);
    }
}
