<?php

namespace Blog\Http\Controllers;

use Blog\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(3);

        return view('home', [
            'posts' => $posts,
        ]);
    }
}
