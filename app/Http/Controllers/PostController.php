<?php

namespace Blog\Http\Controllers;

use Blog\Post;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show a post
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, $id = null)
    {
        $post = Post::find($id) ?? new Post();
        return view('post', [
            'post' => $post,
        ]);
    }

    /**
     * Create a post
     *
     * @return \Illuminate\Http\Response
     */
    public function post(Request $request)
    {
        $request->validate($this->rules);

        $post = Post::create($request->all());
        return redirect()->route('post', ['id' => $post->getId()]);
    }

    /**
     * Update a post
     *
     * @return \Illuminate\Http\Response
     */
    public function patch(Request $request, $id)
    {
        $request->validate($this->rules);

        $post = Post::findOrFail($id);
        $post->update($request->all());
        return redirect()->route('post', ['id' => $post->getId()]);
    }

    /**
     * Delete an existing post
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        Post::findOrFail($id)->delete();
        return Redirect::back()->withInput();
    }

    protected $rules = [
        'title' => 'required|:mongodb.posts|max:255|min:5',
        'description' => 'required|:mongodb.posts|min:24',
        'content' => 'required|:mongodb.posts|min:48',
        'coverurl' => ':mongodb.posts|url',
    ];
}
