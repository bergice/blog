<?php

return [

    'title' => 'Title',
    'content' => 'Content',
    'description' => 'Description',
    'save' => 'Save',
    'delete' => 'Delete',
    'coverurl' => 'Cover URL',
    'coverurl_placeholder' => 'Paste a URL to show as a cover on this article',
    'description_placeholder' => 'Place your description of this post here',
    'title_placeholder' => 'Enter your title here',
    'create_message' => 'Click here to create a new post',
    'no_posts' => 'There are no posts!',

];
