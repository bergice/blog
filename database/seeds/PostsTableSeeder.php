<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('mongodb')->collection('posts')->delete();

        DB::connection('mongodb')->collection('posts')->insert([
            'title' => "7 Secrets from the World’s Top Travel Bloggers",
            'description' => "Becoming a globe-trotting blogger sounds intriguing in every way. Imagine being able to see the world, all the while earning an income? It’s like being on a lifetime vacation with the perks of earning enough to pay for your travel bills.",
            'content' => "<p>Becoming a globe-trotting blogger sounds intriguing in every way. Imagine being able to see the world, all the while earning an income? It&rsquo;s like being on a lifetime vacation with the perks of earning enough to pay for your travel bills.<br />
How do travel bloggers do it? If this is something you aspire to become, then the best way to learn is from those who are already doing it. You can find plenty of individuals and couples on Instagram who has blogs&nbsp;<a href=\"https://www.articlecity.com/blog/5-best-cheap-holiday-destinations-consider-year/\">documenting their travels</a>.</p>

<h2>1. Go Incognito When Searching for Flights</h2>

<p>You&rsquo;re going to book countless flights while on your travels local and abroad. This means you&rsquo;ll need to save as much as you can when booking your tickets.</p>

<p>The way around this is to switch your browser to incognito mode so the airliners are unable to do so. This will allow you to shop around for tickets and double check prices without worry of price hikes.</p>

<h2>2. Travel Bloggers Bring Along Comfort Items</h2>

<p>Now, there are various tools you can use to help make your travels more comfortable. This is especially important when you&rsquo;re traveling with children. For instance, you can allow your children to bring along a toy or stuffed animal.</p>

<h2>3. Don&rsquo;t Overpack</h2>

<p>You never want to pack too many items when you&rsquo;re traveling. Not only will it weigh you down, but it&rsquo;ll empty your wallet. You have to pay for extra luggage that&rsquo;s not qualified as a carry-on, so more fees to worry about.</p>

<p>You&rsquo;ll also be happy you don&rsquo;t have to wait around in long lines for your luggage. Too often people lose their baggage because of airliner mistakes.</p>

<h2>4. Download All Maps Ahead of Time</h2>

<p>You want your maps to be downloaded in advance and in offline mode. This way, you can access them in the event you don&rsquo;t have cell phone data or Wi-Fi access.</p>

<h2>5. Give the Local Cuisine a Try</h2>

<p>There&rsquo;s no better way to experience a culture than to head right to their food markets and restaurants. The types of foods eaten in a particular country is sometimes a complete 360 from what you&rsquo;re accustomed to in your home country.</p>

<h2>6. Get a Filtered Water Bottle</h2>

<p>There are two reasons you want to go this route. One, filtered water bottles will ensure your safety, since you&rsquo;ll drink from tap water quite often in certain foreign countries. Then you can also save money on having to buy bottled water during your stay.</p>

<h2>7. Bring Hard and Soft Copies of Your IDs</h2>

<p>The last thing you need to happen is for you to lose your passport and ID while in a foreign country. It can take weeks, if not months, to sort things out, depending on where you&rsquo;re at.This is definitely the best friend of travel bloggers.</p>
",
            'coverurl' => "https://www.articlecity.com/wp-content/uploads/2018/02/Travel-Bloggers.jpeg",
            'seed' => true,
            'active' => 1,
        ]);

        DB::connection('mongodb')->collection('posts')->insert([
            'title' => "4WD Off Roading Tips Every Beginner Needs to Know",
            'description' => "Most people who own a 4WD vehicle never take it off the road. They buy the vehicle with 4WD for higher resale or maybe to drive to and from work in the snow. If you’re ready for some real adventure, it’s time to take it off roading.",
            'content' => "<p>Most people who own a 4WD vehicle never take it off the road. They buy the vehicle with 4WD for&nbsp;<a href=\"http://www.businessfleet.com/blog/auto-focus/story/2015/02/are-4x4-pickups-worth-the-extra-cost.aspx\" onclick=\"javascript:window.open('http://www.businessfleet.com/blog/auto-focus/story/2015/02/are-4x4-pickups-worth-the-extra-cost.aspx'); return false;\">higher resale</a>&nbsp;or maybe to drive to and from work in the snow. If you&rsquo;re ready for some real adventure, it&rsquo;s time to take it off roading.</p>

<h2>Understand the Basics</h2>

<p>If you&rsquo;ve never been 4WD off roading, it&rsquo;s important to learn some of the basic terminologies before you even climb behind the wheel.</p>

<h3>4X4 High</h3>

<p>All-purpose 4WD mode. It differs from standard 2WD in that all 4 tires are powered and engaged.</p>

<h3>4X4 Low</h3>

<p>4WD mode with a lower gear engaged. Your top speed will be lower because you&rsquo;ll have higher torque in the wheels. Used in more difficult terrains and in helping you to get &ldquo;unstuck.&rdquo;</p>

<h2>Expect the Unexpected</h2>

<p>Once you know the basic terminology, it&rsquo;s time to start prepping your vehicle to go 4WD off roading. Off roading vets will tell you to always expect, and prepare, for the unexpected. Here&rsquo;s a quick list of items to have with you when you set out on your adventure:</p>

<ul>
	<li>Full tank of gas</li>
	<li>Tow ropes</li>
	<li>Portable air compressor</li>
	<li>Spare tire(s)</li>
	<li>First aid kit</li>
	<li>Shovel</li>
	<li>Spare tanks of water</li>
	<li>Two-way radios (often no cell service where you&rsquo;re headed)</li>
	<li>High-lift jack</li>
</ul>

<p>If you have these items on hand, you&rsquo;ll normally be able to get yourself out of any trouble you may run into.</p>

<h2>Prepare Your Tires</h2>

<p>Tires are the single most important piece of gear when you&rsquo;re 4WD off roading. Ensuring that you have the proper tires and that they&rsquo;re prepared for your journey are very important to ensure you have a safe trip.First, make sure you&nbsp;<a href=\"https://www.articlecity.com/blog/when-to-change-tyres/\">change your tires</a>&nbsp;to all-terrain tires. The tires your vehicle came with are optimized for road driving. They will not hold up offroad, so invest in some good all-terrain tires that will help you to manage your vehicle off the beaten path.The next question that 4WD off roading beginners often ask:&nbsp;<a href=\"https://www.4wdsupacentre.com.au/news/should-you-lower-your-tyre-pressures-when-you-take-your-4wd-offroad/\" onclick=\"javascript:window.open('https://www.4wdsupacentre.com.au/news/should-you-lower-your-tyre-pressures-when-you-take-your-4wd-offroad/'); return false;\">Should You Lower Your Tire Pressures When You Take Your 4WD Offroad?</a>&nbsp;The short answer is yes. But you&rsquo;ll need a tire gauge and deflator to help you achieve the correct pressure.</p>
",
            'coverurl' => "https://www.articlecity.com/wp-content/uploads/2018/02/4wd-off-roading.jpeg",
            'seed' => true,
            'active' => 1,
        ]);

        DB::connection('mongodb')->collection('posts')->insert([
            'title' => "How to Choose The Right Wall Decor For Your Home",
            'description' => "The way you decorate your home can be a reflection of your personality. You want to pick items that enhance the space you’re working within, but also pieces that match your unique style.",
            'content' => "<p>The way you decorate your home can be&nbsp;<a href=\"https://www.huffingtonpost.com/thomas-b-trafecanty/what-does-my-decor-say-ab_b_10535850.html\" onclick=\"javascript:window.open('https://www.huffingtonpost.com/thomas-b-trafecanty/what-does-my-decor-say-ab_b_10535850.html'); return false;\">a reflection of your personality.&nbsp;</a>You want to pick items that enhance the space you&rsquo;re working within, but also pieces that match your unique style.</p>

<p>Finding wall decor that strikes that balance can feel daunting, especially as home renovation shows and DIY blogs promote a picture perfect ideal.</p>

<p>It&rsquo;s important to keep in mind that you don&rsquo;t need to make your home look like a picture out of a magazine. You just need to make it a space that is meaningful to you.</p>

<p>Whether you&rsquo;re taking on&nbsp;<a href=\"https://www.articlecity.com/blog/5-clever-home-hacks-make-diy-remodeling-easier/\">a full remodel</a>&nbsp;or just wanting to spruce up your walls, the right decor can make all the difference.</p>

<p>Below, we&rsquo;re sharing easy tips for picking the right wall decor for your home. Read on to learn more, and start decorating!</p>

<h2>1. Choose Something Meaningful</h2>

<p>There are a lot of reasons why you might want to incorporate your personal values and belief into your wall decor. Choosing a piece that is personally meaningful can help you maintain a positive attitude.</p>

<p>Choose a piece of&nbsp;<a href=\"https://ikonick.com/\" onclick=\"javascript:window.open('https://ikonick.com/'); return false;\">motivational art</a>&nbsp;that is meaningful. Maybe it&rsquo;s a favorite saying or phrase. Maybe it&rsquo;s an image that uplifts you. Whatever it is, put it in a place where you will see it every day and let it inspire you.</p>

<h2>2. Frame Your Memories</h2>

<p>Filling your walls with framed pictures is another great way to share who you are through your decor.</p>

<p>Great pictures from past vacations or unique portraits of friends and family are great options for any room. Seeing them can be a reminder of wonderful past experiences or can make you feel closer to loved ones who live far away.</p>

<p>The best thing about this kind of decor is that it will be personal to you. No one else will have those same photos and it&rsquo;s your way to make your unique mark on your space.</p>

<h2>3. Bring Your Home to Life</h2>

<p>Small plants add both color and liveliness to any space. There are several interesting ways that you can incorporate plants into your decor.</p>

<p>For plants like succulents, which grow in smaller pots, consider building a shelf on one wall, and lining all your pots up side by side. If you prefer larger, hanging plants, you can screw decorative hooks or nails into your walls.</p>

<p>Besides adding a clean, natural look to any room, taking care of houseplants has been linked to&nbsp;<a href=\"https://gardencollage.com/heal/mind-spirit/houseplants-improve-mental-health/\" onclick=\"javascript:window.open('https://gardencollage.com/heal/mind-spirit/houseplants-improve-mental-health/'); return false;\">improved mental health</a>&nbsp;and a better mood.</p>

<h2>4. Add An Accent Color</h2>

<p>If you prefer a more minimal look, you may not want to add a lot of different elements to your walls. In that case, painting your walls may be a good option.</p>

<p>A subtle color throughout a room can be calming, but if you prefer to be a little bolder, choose a bright color and paint one wall to be the focal point of the room.</p>

<h2>Ready to Start Choosing Wall Decor?</h2>

<p>No matter what your personal style is, the way you decorate your home should be a reflection of that. With these tips in mind, you can start shopping for the elements that will make your home feel uniquely yours.</p>
",
            'coverurl' => "https://www.articlecity.com/wp-content/uploads/2018/02/wall-decor.jpeg",
            'seed' => true,
            'active' => 1,
        ]);

        DB::connection('mongodb')->collection('posts')->insert([
            'title' => "The 3 Best Cheap Holiday Destinations to Consider This Year",
            'description' => "“Once a year, go someplace you’ve never been before” – Dalai Lama.

This ancient quote has been trending on social media recently. Why?According to research, millennials are traveling a considerable amount more than previous generations. More than ever before, many are taking vacations to “off the beaten track” locations around the world.",
            'content' => "<p>&ldquo;Once a year, go someplace you&rsquo;ve never been before&rdquo; &ndash; Dalai Lama.</p>

<p>This ancient quote has been trending on social media recently. Why?<a href=\"http://reports.mintel.com/display/748294/?__cc=1\" onclick=\"javascript:window.open('http://reports.mintel.com/display/748294/?__cc=1'); return false;\">According to research</a>, millennials are traveling a considerable amount more than previous generations. More than ever before, many are taking vacations to &ldquo;off the beaten track&rdquo; locations around the world.</p>

<h2>1. Portugal</h2>

<p>Old towns, lagoon islands, salt pans, ancient fortresses, cliff top views and sandy surf beaches are some of the many places you can visit when heading to Portugal.</p>

<p>Accommodation varies in price. But there are many B&amp;Bs, vacation rentals, or cheaper hotels to choose from.</p>

<p>If you head to a luxury 5-star, all-inclusive hotel, you&rsquo;ll not only spend megabucks but you might miss out on the local experience. Go for cheaper accommodation and spend your time hanging out with the locals for an authentic experience.Many budget airlines travel to Portugal, some even with regular bargains and deals, which means your flight might be even cheaper! To travel around, you don&rsquo;t need your own car as there are plenty of buses and trains that run regularly at fair prices.</p>

<h2>2. Indonesia</h2>

<p>Indonesia, although a long-haul destination, is extremely popular thanks to low travel costs and cheap activities.Made up of thousands of islands, Indonesia is full of rich culture and beauty. Indonesia is very varied, with monkey jungles, religious temples, volcanoes, shipwrecks, and mountains. There are also many fun things to do, from snorkeling and scuba diving to white water rafting and quad biking.</p>

<p>Return flights to Indonesia vary during the time of year but are generally under $500. Most of the main islands have buses to travel around, but a common method of travel is by ferry (from island to island).</p>

<p>Food in Indonesia is a similar price to many other Asian countries, such as Thailand. But the taste is obviously different wherever you go. The national dish is Nasi Goreng, which can be bought for just a couple of dollars.</p>

<h2>3. Zanzibar</h2>

<p>Zanzibar boasts a variety of gorgeous attractions. From pristine beaches with coral reefs to spice tours and old forts, Zanzibar is an island full of beautiful contrasts which makes it one of the best cheap holiday destinations.</p>

<p>Although the flights may be a bit more pricey, your dollars will stretch far while you are there.</p>

<p>The cheapest and most convenient way to travel is using the &ldquo;dala dala&rdquo; which is a shared minibus/taxi. There are several around, and you&rsquo;ll get a real authentic experience if you use one. It&rsquo;s not overly expensive to hire a personal driver either when compared to other countries.</p>

<p>Accommodation can also be done on a budget, with many vacation rentals and hotels being great value for money.</p>

<p>Zanzibar has a variety of foods to try, and most are for seriously cheap prices too. Make sure to try their famous Pilaf rice and their meat kebabs. Don&rsquo;t forget to try some sugar cane juice too!</p>
",
            'coverurl' => "https://www.articlecity.com/wp-content/uploads/2018/02/best-cheap-holiday-destinations.jpeg",
            'seed' => true,
            'active' => 1,
            '_id' => "5a9d0f8e9a892012bb2ed465",
        ]);

    }
}
