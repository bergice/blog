<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/post/{id?}', 'PostController@get')->name('post');
Route::post('/post', 'PostController@post')->middleware('auth');
Route::patch('/post/{id?}', 'PostController@patch')->middleware('auth');
Route::delete('/post/{id}', 'PostController@delete')->middleware('auth');
